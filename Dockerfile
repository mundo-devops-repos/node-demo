FROM node:15.11.0-alpine3.13

ENV PORT=8080

RUN apk update && apk add --no-cache curl &&\
    rm -rf /var/cache/apk/*

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE ${PORT}

HEALTHCHECK --interval=10s --timeout=3s --retries=2 \
  CMD curl -f http://localhost:${PORT}/ || exit 1

CMD [ "node", "server.js" ]
